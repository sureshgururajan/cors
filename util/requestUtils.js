function getClientDetails(requestID, request) {
  return {
    type: "request",
    remoteIP: request.ip,
    AdditionalXFF: request.ips,
    path: request.path,
    query: request.query,
    requestID: requestID,
    hostname: request.hostname
  }
}


function getResponseMessage(requestID, statusCode, message) {
  return {
    type: "response",
    statusCode,
    message,
    requestID
  }
}


function getCustomerResponseMessage(requestID, statusCode, message) {
  return {
    statusCode,
    message,
    requestID
  }
}


module.exports = { getClientDetails, getResponseMessage, getCustomerResponseMessage }
