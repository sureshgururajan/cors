const { log } = require('./logUtils.js')
const { getClientDetails } = require('./requestUtils.js')


function printRequestDetails(requestID, request) {
  let message = JSON.stringify(getClientDetails(requestID, request))
  log(`${message}`)
}


function printResponseDetails(responseJSON) {
  let responseMessage = JSON.stringify(responseJSON)
  log(`${responseMessage}`)
}


module.exports = { printRequestDetails, printResponseDetails }
