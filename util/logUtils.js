function log(message) {
  console.log(`${new Date().toISOString()} [INFO] ${message}`)
}

module.exports = { log }
